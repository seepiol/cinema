/*
    Jacopo Moioli

    MoioLib

    A based general-purpose C library
*/



// A drop-in replacement for stdio's deprecated function "gets"
// Use just for educational purposes, because it can be used to create buffer overflows
void gets(char string[]){
    int i=0;
    char c;

    do{
        c=getchar();
        if(c!='\n'){
            string[i]=c;
        }else{
            string[i]='\0';
        }
        i++;
    }while(c!='\n');
}
