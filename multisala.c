/* Gestione Cinema Multisala
 * 24/03/2021 Jacopo Moioli
 * 3Bi IIS Badoni
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

// pulizia buffer multipiattaforma
int ch;
#define clbfr while((ch = getchar()) != '\n' && ch != EOF)

// determinare piattaforma 
#if defined(_WIN32) || defined (_WIN64)
    #include <winsock2.h>
    #include <Ws2tcpip.h>
#elif defined(__linux__) || defined(__APPLE__)
    #include "moiolib.h"
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
#endif

/* server socket durata */
#define PORTA 6940// porta per la connessione tramite socket al server di durata
#define IP "45.77.63.134"
// vedi readme.md#durata

/* Struttura Dati */
#define NPROIEZIONI 200
#define LEN_TITOLO 100

// Modello Proiezione
int  giornata[NPROIEZIONI];
char titolo[NPROIEZIONI][LEN_TITOLO+1];
char sala[NPROIEZIONI][4];
char orario_inizio[NPROIEZIONI][6];  
char orario_fine[NPROIEZIONI][6];  
int  posti_liberi[NPROIEZIONI];

/* Variabili */
int cont_proiezioni=0;


/* Prototipi Funzioni */
int  visualizza(int b_richiedi_proiezione, char messaggio[]);
void test_riempimento();
int  valida_orario(char orario[]);
int  aggiungi();
int  controllo_spazio();
int  lanciatore();
int  modifica();
int  prenotazioni();
int  elimina();
int  menu_ricerca();
int  ricerca_titolo();
int  ricerca_sala();
int  ricerca_orario();
int  controlla_sovrapposizioni(char a_orario_inizio[], char a_orario_fine[], char a_sala[], int esclusa);
int  controlla_orario(char a_orario_inizio[], char a_orario_fine[]);
int  ordina();
int  val_data(char a_orario[]);
int  durata_to_fine(char a_orario_inizio[], int minuti_durata, char a_orario_fine[]);
int  fsearch(char query[]);
void clean();

/* Funzioni */

int main(){
    test_riempimento();
    lanciatore();
    return 0;
}


/*
 * Menu
 */
int lanciatore(){
    char scelta;

    while(1){

        clean();
        printf(
            "\n▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓"
            "\n▓▓   __  __       _ _   _ ____        _        ▓▓"  
            "\n▓▓  |  \\/  |_   _| | |_(_) ___|  __ _| | __ _  ▓▓" 
            "\n▓▓  | |\\/| | | | | | __| \\___ \\ / _` | |/ _` | ▓▓"
            "\n▓▓  | |  | | |_| | | |_| |___) | (_| | | (_| | ▓▓"
            "\n▓▓  |_|  |_|\\__,_|_|\\__|_|____/ \\__,_|_|\\__,_| ▓▓"
            "\n▓▓                                             ▓▓"
            "\n▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓"
            "\n▓▓                                             ▓▓"
            "\n▓▓      [1, v]: Visualizza Proiezioni          ▓▓"
            "\n▓▓      [2, s]: Ricerca Avanzata               ▓▓"
            "\n▓▓      [3, +]: Aggiungi Proiezione            ▓▓"
            "\n▓▓      [4, m]: Modifica Proiezione            ▓▓"
            "\n▓▓      [5, p]: Aggiungi Prenotazioni          ▓▓"
            "\n▓▓      [6, d]: Elimina Proiezione             ▓▓"
            "\n▓▓      [0, q]: Esci                           ▓▓"
            "\n▓▓                                             ▓▓"
            "\n▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓"
           ); 

        // controllo input e ripetizione se non valido
        printf("\n\n[Selezione]> ");
        scanf("%c", &scelta);
        while(
        scelta!='0' &&
        scelta!='1' &&
        scelta!='2' &&
        scelta!='3' &&
        scelta!='4' &&
        scelta!='5' &&
        scelta!='6' &&
        scelta!='v' &&
        scelta!='s' &&
        scelta!='+' &&
        scelta!='m' &&
        scelta!='p' &&
        scelta!='d' &&
        scelta!='q' &&
        scelta!='o'
        ){
            printf("\nATTENZIONE: Opzione non valida.");
            printf("\n[Selezione]> ");
            clbfr;
            scanf("%c", &scelta);
        }
        
        clbfr;
        clean();
        // valutazione scelta
        switch(scelta){
            case '1':
                visualizza(0,"");
                break;
            case 'v':
                visualizza(0,"");
                break;
            case '2':
            case 's':
                menu_ricerca();
                break;
            case '3':
            case '+':
                aggiungi();
                break;
            case '4':
            case 'm':
                modifica();
                break;
            case '5':
            case 'p':
                prenotazioni();
                break;
            case '6':
            case 'd':
                elimina();
                break;
            case '0':
            case 'q':
                return 0;
            case 'o':
                ordina();
                break;
        }

        printf("\nPremi INVIO per tornare al menu\n");
        clbfr;
    }

}


int menu_ricerca(){
    char scelta;

    printf("[1, t]: Ricerca per Titolo"
           "\n[2, s]: Ricerca per Sala"
           "\n[0, q]: Indietro"
           "\n\n[Selezione]>"
          );

    scanf("%c", &scelta);
   
    while(
        scelta!='1'&&
        scelta!='t'&&
        scelta!='2'&&
        scelta!='s'&&
        scelta!='0'&&
        scelta!='q'
        ){
        printf("\nATTENZIONE: Opzione non valida.");
        printf("\n[Selezione]> ");
        clbfr;
        scanf("%c", &scelta);
    }

    switch(scelta){
        case '1':
            ricerca_titolo();
            break;
        case 't':
            ricerca_titolo();
            break;
        case '2':
        case 's':
            ricerca_sala();
            break;
        case 'q':
        case '0':
            return 0;
            break;
    }
    //clbfr;
    //clean();
            

}


int ricerca_titolo(){
    char query[LEN_TITOLO];
    int i;
    int risultati[NPROIEZIONI];
    int cont_risultati=0;

    printf("Inserisci la query di ricerca (q=menu): ");
    clbfr;
    gets(query);

    if(!strcmp(query, "q")){
        return 0;
    }
    
    clean(); 
    // salva l'indice dei risultati in un array
    for(i=0; i<cont_proiezioni; i++){
        if(strstr(titolo[i], query)!=NULL){
            risultati[cont_risultati]=i;
            cont_risultati++;
        }
    }

    /*
     * schermata visualizzazione risultati
     */

    clean();
    
    if(cont_risultati==0){
        // no risultati
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║           Nessuna Proiezione Trovata           ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
            return 0;

    }else if (cont_risultati==1){
        // un risultato
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║            Una Proiezione Trovata              ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
    }else{
        // >1 risultato
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║             %2d Proiezioni trovate              ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
    }

    for(i=0; i<cont_risultati; i++){
        printf(
            "\n┌────────────────────────────────────────────────┐"
            "\n│                  RISULTATO %-3d                 │"
            "\n├──────────────────┬─────────────────────────────┤"
            "\n│ Titolo:          │ %-27s │"    
            "\n├──────────────────┼─────────────────────────────┤"
            "\n│ Sala:            │ %-27s │"    
            "\n├──────────────────┼─────────────────────────────┤"
            "\n│ Orario:          │ %s -> %s              │"    
            "\n├──────────────────┼─────────────────────────────┤"
            "\n│ Posti Liberi:    │ %-27d │"    
            "\n└──────────────────┴─────────────────────────────┘",
            i+1,
            titolo[risultati[i]],
            sala[risultati[i]],
            orario_inizio[risultati[i]], orario_fine[risultati[i]],
            posti_liberi[risultati[i]]
            
        );
    }      

    return 0;
}


int ricerca_sala(){
    char query[4];
    int risultati[NPROIEZIONI];
    int i;
    int cont_risultati=0;

    printf("Inserisci il nome della sala (q=menu): ");
    clbfr;
    gets(query);

    if(!strcmp(query, "q")){
        return 0;
    }

    clean();
    // salva l'indice dei risultati in un array
    for(i=0; i<cont_proiezioni; i++){
        if(!strcmp(sala[i], query)){
            risultati[cont_risultati]=i;
            cont_risultati++;
        }
    }

    /*
     * schermata visualizzazione risultati
     */

    clean();
    
    if(cont_risultati==0){
        // no risultati
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║           Nessuna Proiezione Trovata           ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
            return 0;

    }else if (cont_risultati==1){
        // un risultato
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║            Una Proiezione Trovata              ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
    }else{
        // >1 risultato
        printf(
            "╔════════════════════════════════════════════════╗"
            "\n║                   MULTISALA                    ║"
            "\n╠════════════════════════════════════════════════╣"
            "\n║              RISULATI DI RICERCA               ║" 
            "\n║             %2d Proiezioni trovate              ║" 
            "\n╙────────────────────────────────────────────────╜",
            cont_risultati
            );
    }


    for(i=0; i<cont_risultati; i++){
        printf(
            "\n┌────────────────────────────────────────────────┐"
            "\n│                  RISULTATO %-3d                 │"
            "\n├──────────────────┬─────────────────────────────┤"
            "\n│ Titolo:          │ %-27s │"    
            "\n├──────────────────┼─────────────────────────────┤"
            "\n│ Orario:          │ %s -> %s              │"    
            "\n├──────────────────┼─────────────────────────────┤"
            "\n│ Posti Liberi:    │ %-27d │"    
            "\n└──────────────────┴─────────────────────────────┘",
            i+1,
            titolo[risultati[i]],
            orario_inizio[risultati[i]], orario_fine[risultati[i]],
            posti_liberi[risultati[i]]
            
        );
    }      

    return 0;

}


/*
 * Stampa le proiezioni
 * 
 * Parametri
 * b_richiedi_proiezione (int): valore booleano. 1=richiesta input numero proiezione
 * messaggio (str): messaggio in input. ignorato se b_richiedi_posizione = 0
 */
int visualizza(int b_richiedi_proiezione, char messaggio[]){
    
    int n_proiezione;

    printf(
        "╔════════════════════════════════════════════════╗"
        "\n║                   MULTISALA                    ║"
        "\n╠════════════════════════════════════════════════╣"
        "\n║           %d proiezioni pianificate             ║" 
        "\n╙────────────────────────────────────────────────╜",
        cont_proiezioni
        );

    if(cont_proiezioni!=0){  // controllo presenza proiezioni
        
        // ordina vettori
        ordina();
       
        // a dev non piace la dichiarazione di variabili nel for
        int i;
        for(i=0;i<cont_proiezioni;i++){
            printf(
                "\n┌────────────────────────────────────────────────┐"
                "\n│                 PROIEZIONE %-3d                 │"
                "\n├──────────────────┬─────────────────────────────┤"
                "\n│ Titolo:          │ %-27s │"    
                "\n├──────────────────┼─────────────────────────────┤"
                "\n│ Sala:            │ %-27s │"    
                "\n├──────────────────┼─────────────────────────────┤"
                "\n│ Orario:          │ %s -> %s              │"    
                "\n├──────────────────┼─────────────────────────────┤"
                "\n│ Posti Liberi:    │ %-27d │"    
                "\n└──────────────────┴─────────────────────────────┘",
                i+1,
                titolo[i],
                sala[i],
                orario_inizio[i], orario_fine[i],
                posti_liberi[i]
                
            );
        }

        // se richiesto la selezione di una proiezione
        if(b_richiedi_proiezione){
            printf("\n\n%s", messaggio);
            scanf("%d", &n_proiezione);
            if(n_proiezione=='q'){
                return 0;
            }
            while(n_proiezione-1<0||n_proiezione-1>=cont_proiezioni){
                printf("ATTENZIONE: Opzione non valida.\n");
                printf("\n\n%s", messaggio);
                scanf("%d", &n_proiezione);
            }
            clbfr;
            n_proiezione-=1;
            printf("\n");
            return n_proiezione;
        }else{
            return 0;
        }
    }else{
        printf("\nNessuna Proiezione Programmata\n");
        return -1;
    }
}


int prenotazioni(){
    int n_proiezione;
    char i_n_prenotazioni[5];
    int n_prenotazioni;
    int diff;
    char accetta_riduzione;
    
    n_proiezione=visualizza(1, "Inserisci il numero della proiezione prenotata: ");
    if(n_proiezione==-1){
        return 0;
    }else{
        while(1){
            
            if(posti_liberi[n_proiezione]==0){
                printf("\nPOSTI ESAURITI\n");
                return 0;
            }

            printf("\n[%d posti disponibili]\nInserisci il numero di prenotazioni (invio=1, 'q'=menu): ", posti_liberi[n_proiezione]);
            gets(i_n_prenotazioni);
            // prenotazione unica
            if(!strcmp(i_n_prenotazioni,"")){
                n_prenotazioni=1;
            }else if(!strcmp(i_n_prenotazioni,"q")){
                return 0;
            }else{ // + prenotazioni
                n_prenotazioni = atoi(i_n_prenotazioni); // conversione str->int

                // controllo prenotazioni
                while(n_prenotazioni<1){
                    printf("\nATTENZIONE: valore non valido. Per rimuovere le prenotazioni, tornare al menu e selezionare \"modifica proiezione\"\n");
                    printf("[%d posti disponibili]\nInserisci il numero di prenotazioni (invio=1, 'q'=menu): ", posti_liberi[n_proiezione]);
                    gets(i_n_prenotazioni); 
                    if(!strcmp(i_n_prenotazioni,"")){ // se invio
                        n_prenotazioni=1;
                    }else if(!strcmp(i_n_prenotazioni,"q")){ // se q
                        return 0;
                    }else{
                        n_prenotazioni = atoi(i_n_prenotazioni);
                    }
                }
            }

            if(n_prenotazioni>posti_liberi[n_proiezione]){
                diff=n_prenotazioni-posti_liberi[n_proiezione];
                printf("\nI posti in sala non sono sufficenti per il numero di prenotazioni inserito.\nAccettare le prime %d prenotazioni? (N/s): ", n_prenotazioni-diff);
                scanf("%c", &accetta_riduzione);
                clbfr;
                if(accetta_riduzione=='s'||accetta_riduzione=='S'){
                    // se utente inserisce 50 ma posti disponibili 40, vengono occupati 40 posti e gli altri 10 vengono ignorati
                    n_prenotazioni = n_prenotazioni-diff;
                }else{
                    n_prenotazioni = 0;
                }
            }



            if(n_prenotazioni==1){
                printf("[Prenotazione Salvata]\n");
            }else{
                printf("[%d Prenotazioni Salvate]\n", n_prenotazioni);
            }
            posti_liberi[n_proiezione]-=n_prenotazioni;

        }
    }
}


int modifica(){
    // variabili per salvataggio temporaneo delle modifiche
    int n_proiezione;
    char i_titolo[LEN_TITOLO+1];
    char i_sala[4];
    char i_orario_inizio[6];
    char i_orario_fine[6];
    char i_posti_liberi[5];
    
    n_proiezione=visualizza(1, "Inserisci il numero della proiezione che desideri modificare: ");
    if(n_proiezione==-1){
        return 0;
    }else{
        
        printf("\nInserire 'q' in qualsiasi momento per annullare la modifica e tornare al menu\n\n");

        /*
         * Modifica del titolo del film della proiezione
         */
        printf("Inserisci il titolo del film (INVIO=%s): ", titolo[n_proiezione]);
        gets(i_titolo);
        if(!strcmp(i_titolo, "q"))  // se inserito q, ritorna al menu
            return 0;
        
        /* 
         * Modifica sala proiezione
         */
        printf("Inserisci il nome della sala (INVIO=%s): ", sala[n_proiezione]);
        gets(i_sala);
        if(!strcmp(i_sala, "q"))   
            return 0;

        /*
         * Modifica orario di inizio
         */
        printf("Inserisci l'orario di inizio della proiezione (INVIO=%s): ", orario_inizio[n_proiezione]);
        gets(i_orario_inizio);
        if(!strcmp(i_orario_inizio, "q"))   
            return 0;
        while(valida_orario(i_orario_inizio)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di inizio della proiezione (INVIO=%s): ", orario_inizio[n_proiezione]);
            gets(i_orario_inizio);
            if(!strcmp(i_orario_inizio, "q"))
                return 0;
        }

        /*
         * Modifica orario di fine 
         */
        printf("Inserisci l'orario di fine della proiezione (INVIO=%s): ", orario_fine[n_proiezione]);
        gets(i_orario_fine);
        if(!strcmp(i_orario_fine, "q"))   
            return 0;
        while(valida_orario(i_orario_fine)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di fine della proiezione (INVIO=%s): ", orario_fine[n_proiezione]);
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))
                return 0;
        }

        while(controlla_sovrapposizioni(i_orario_inizio, i_orario_fine, i_sala, n_proiezione)){
            
            printf("\nATTENZIONE: nell'intervallo orario inserito è gia presente un'altra proiezione nella stessa sala.\nInserire un altro orario\n");

            printf("Inserisci l'orario di inizio della proiezione (INVIO=%s): ", orario_inizio[n_proiezione]);
            gets(i_orario_inizio);
            if(!strcmp(i_orario_inizio, "q"))   
                return 0;
            while(valida_orario(i_orario_inizio)==-1){
                printf("\nATTENZIONE: orario non valido.\n");
                printf("Inserisci l'orario di inizio della proiezione (INVIO=%s): ", orario_inizio[n_proiezione]);
                gets(i_orario_inizio);
                if(!strcmp(i_orario_inizio, "q"))
                    return 0;
            }

            /*
             * Modifica orario di fine 
             */
            printf("Inserisci l'orario di fine della proiezione (INVIO=%s): ", orario_fine[n_proiezione]);
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))   
                return 0;
            while(valida_orario(i_orario_fine)==-1){
                printf("\nATTENZIONE: orario non valido.\n");
                printf("Inserisci l'orario di fine della proiezione (INVIO=%s): ", orario_fine[n_proiezione]);
                gets(i_orario_fine);
                if(!strcmp(i_orario_fine, "q"))
                    return 0;
            }
        }

        /*
         * Modifica numero posti liberi
         */
        printf("Inserisci il numero di posti liberi rimanenti (INVIO=%d): ", posti_liberi[n_proiezione]);
        gets(i_posti_liberi);
        if(!strcmp(i_posti_liberi, "q"))   
            return 0;
        while(atoi(i_posti_liberi)<0){
            printf("\nATTENZIONE: Numero di posti liberi non valido\nIl valore deve essere positivo\n");
            printf("Inserisci il numero di posti liberi rimanenti (INVIO=%d): ", posti_liberi[n_proiezione]);
            gets(i_posti_liberi);
            if(!strcmp(i_posti_liberi, "q"))   
                return 0;
        }
            


        // modifica delle info

        //se il titolo è diverso da stringa vuota si cambia
        if(strcmp(i_titolo, "")){
           strcpy(titolo[n_proiezione],i_titolo); 
        }

        if(strcmp(i_sala, "")){
           strcpy(sala[n_proiezione], i_sala);
        }
        
        if(strcmp(i_orario_inizio, "")){
            strcpy(orario_inizio[n_proiezione], i_orario_inizio);
        }

        if(strcmp(i_orario_fine, "")){
            strcpy(orario_fine[n_proiezione], i_orario_fine);
        }

        if(strcmp(i_posti_liberi, "")){
            posti_liberi[n_proiezione] = atoi(i_posti_liberi);
        }


        // ordina vettori
        ordina();

    }
}


/*
 * Aggiunta di una proiezione da parte dell'utente
 */
int aggiungi(){
    // controlla se ci sia abbastanza spazio nei vettori
    if(!controllo_spazio()){
        return -1; //errore
    }

    // variabili per input
    char i_titolo[LEN_TITOLO+1];
    char i_sala[4];
    char i_orario_inizio[6];
    char i_orario_fine[6];
    char i_posti_liberi[6];
    char i_durata_minuti[4];
    int  durata_minuti;
    char automatica;
    
    
    // il salvataggio delle informazioni sull'array viene eseguito alla fine


    printf(
        "╔════════════════════════════════════════════════╗"
        "\n║                   MULTISALA                    ║"
        "\n╚════════════════════════════════════════════════╝"
        );
    
    printf("\nInserire 'q' in qualsiasi momento per \nannullare la creazione e tornare al menu\n\n");


    printf("\nInserisci il titolo del film proiettato: ");
    gets(i_titolo);
    if(!strcmp(i_titolo, "q"))  // se inserito q, ritorna al menu
        return 0;

    printf("Inserisci il nome della sala: ");
    gets(i_sala);
    if(!strcmp(i_sala, "q"))
        return 0;
   
    // con modalità automatica si inserisce la durata del film
    printf("Vuoi provare la modalità automatica sperimentale? N/s: ");
    scanf("%c", &automatica);
    clbfr;
    
    if(automatica=='q'){
        return 0;
    }

    printf("Inserisci l'orario di inizio della proiezione: ");
    gets(i_orario_inizio);
    if(!strcmp(i_orario_inizio, "q"))
        return 0;
    while(valida_orario(i_orario_inizio)==-1){
        printf("\nATTENZIONE: orario non valido.\n");
        printf("Inserisci l'orario di inizio della proiezione: ");
        gets(i_orario_inizio);
        if(!strcmp(i_orario_inizio, "q"))
            return 0;
    }
   
    if(automatica=='s'||automatica=='S'){
        printf("\nAcquisizione durata %s in corso...\n", i_titolo);
        durata_minuti = fsearch(i_titolo);
        if(durata_minuti==-1){
            printf("\nÈ stato riscontrato un errore durante la ricerca del titolo.\nInserirlo manualmente.\n");
            printf("Inserisci la durata del film: ");
            gets(i_durata_minuti);
            if(!strcmp(i_durata_minuti, "q")){
                return 0;
            }
            durata_minuti = atoi(i_durata_minuti);
            durata_to_fine(i_orario_inizio, durata_minuti, i_orario_fine);
        }else{            
            printf("Durata: %d\n", durata_minuti);
            durata_to_fine(i_orario_inizio, durata_minuti, i_orario_fine);
        }
        printf("Orario di termine proiezione: %s\n", i_orario_fine);
    }else{ 
        printf("Inserisci l'orario di termine della proiezione: ");
        gets(i_orario_fine);
        if(!strcmp(i_orario_fine, "q"))
            return 0;
        while(valida_orario(i_orario_fine)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di termine della proiezione: ");
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))
                return 0;
        }
    }
    
    while(controlla_sovrapposizioni(i_orario_inizio, i_orario_fine, i_sala, -1)){
        
        printf("\nATTENZIONE: nell'intervallo orario inserito è gia presente un'altra proiezione nella stessa sala.\nInserire un altro orario\n");

        printf("Inserisci l'orario di inizio della proiezione: ");
        gets(i_orario_inizio);
        if(!strcmp(i_orario_inizio, "q"))
            return 0;
        while(valida_orario(i_orario_inizio)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di inizio della proiezione: ");
            gets(i_orario_inizio);
            if(!strcmp(i_orario_inizio, "q"))
                return 0;
        }
            
        printf("Inserisci l'orario di termine della proiezione: ");
        gets(i_orario_fine);
        if(!strcmp(i_orario_fine, "q"))
            return 0;
        while(valida_orario(i_orario_fine)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di termine della proiezione: ");
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))
                return 0;
        }
    }

    while(controlla_orario(i_orario_inizio, i_orario_fine)){
        
        printf("\nATTENZIONE: L'orario di termine precede l'orario di inizio\nReinserire gli orari di proiezione.\n");

        printf("Inserisci l'orario di inizio della proiezione: ");
        gets(i_orario_inizio);
        if(!strcmp(i_orario_inizio, "q"))
            return 0;
        while(valida_orario(i_orario_inizio)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di inizio della proiezione: ");
            gets(i_orario_inizio);
            if(!strcmp(i_orario_inizio, "q"))
                return 0;
        }
            
        printf("Inserisci l'orario di termine della proiezione: ");
        gets(i_orario_fine);
        if(!strcmp(i_orario_fine, "q"))
            return 0;
        while(valida_orario(i_orario_fine)==-1){
            printf("\nATTENZIONE: orario non valido.\n");
            printf("Inserisci l'orario di termine della proiezione: ");
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))
                return 0;
        }

        while(controlla_sovrapposizioni(i_orario_inizio, i_orario_fine, i_sala, -1)){
            
            printf("\nATTENZIONE: nell'intervallo orario inserito è gia presente un'altra proiezione nella stessa sala.\nInserire un altro orario\n");

            printf("Inserisci l'orario di inizio della proiezione: ");
            gets(i_orario_inizio);
            if(!strcmp(i_orario_inizio, "q"))
                return 0;
            while(valida_orario(i_orario_inizio)==-1){
                printf("\nATTENZIONE: orario non valido.\n");
                printf("Inserisci l'orario di inizio della proiezione: ");
                gets(i_orario_inizio);
                if(!strcmp(i_orario_inizio, "q"))
                    return 0;
            }
                
            printf("Inserisci l'orario di termine della proiezione: ");
            gets(i_orario_fine);
            if(!strcmp(i_orario_fine, "q"))
                return 0;
            while(valida_orario(i_orario_fine)==-1){
                printf("\nATTENZIONE: orario non valido.\n");
                printf("Inserisci l'orario di termine della proiezione: ");
                gets(i_orario_fine);
                if(!strcmp(i_orario_fine, "q"))
                    return 0;
            }
        }
    }
    
    printf("Inserisci Il numero di posti liberi nella sala: ");
    gets(i_posti_liberi);
    if(!strcmp(i_posti_liberi, "q"))
        return 0;
    while(atoi(i_posti_liberi)<0){
        printf("\nATTENZIONE: numero posti liberi non valido.\nIl valore deve essere positivo\n");
        printf("Inserisci Il numero di posti liberi nella sala: ");
        gets(i_posti_liberi);
        if(!strcmp(i_posti_liberi, "q"))
            return 0;
    }

    // salvataggio proiezione
    strcpy(titolo[cont_proiezioni], i_titolo);
    strcpy(sala[cont_proiezioni], i_sala);
    strcpy(orario_inizio[cont_proiezioni], i_orario_inizio);
    strcpy(orario_fine[cont_proiezioni], i_orario_fine);
    posti_liberi[cont_proiezioni]=atoi(i_posti_liberi);
    
    // ordina vettori
    ordina();

    cont_proiezioni++; // incrementa posizione successiva vettori

    return 1;
}


int elimina(){
    int n_proiezione;
    char conferma;
    char conferma_titolo[LEN_TITOLO];
    
    // doppia conferma

    n_proiezione=visualizza(1, "Inserisci il numero della proiezione che desideri eliminare: ");
    if(n_proiezione==-1){
        return 0;
    }else{
        printf("Sei sicuro di cancellare la proiezione numero %d:"
               "\nTitolo: %s"
               "\nSala: %s"
               "\nOrario: %s -> %s",
               n_proiezione+1,
               titolo[n_proiezione],
               sala[n_proiezione],
               orario_inizio[n_proiezione],
               orario_fine[n_proiezione]);
        printf("\n\nN/s: ");
        scanf("%c", &conferma);
        clbfr;
        
        // conferma si/no
        if(conferma!='s'&&conferma!='S'){
            printf("Annullato\n");
            return 0;
        }

        // inserimento titolo come seconda conferma
        printf("\nScrivi esattamente il titolo per confermare la rimozione.\nTitolo: ");
        gets(conferma_titolo);

        if(strcmp(conferma_titolo, titolo[n_proiezione])!=0){
            printf("Verifica Fallita: Rimozione non effettuata\n");
            return 0;
        }

        // la proiezione viene sovrascritta dalla proiezione successiva (shift a sinistra) solo se non è l'ultima proiezione
        if(n_proiezione<cont_proiezioni-1){
            int i;
            for(i=n_proiezione; i<cont_proiezioni-1; i++){
                strcpy(titolo[i], titolo[i+1]);
                strcpy(sala[i], sala[i+1]);
                strcpy(orario_inizio[i], orario_inizio[i+1]);
                strcpy(orario_fine[i], orario_fine[i+1]);
            posti_liberi[i]=posti_liberi[i+1];
        }
        }

        // contatore proiezioni decrementato
        cont_proiezioni--;

    }
}

/*
 * Ordina gli array paralleli per orario di inizio
 * Selection Sort
 */
int ordina(){
    int i,j;
    int min;
    char titolo_tmp[LEN_TITOLO];
    char sala_tmp[4];
    char orario_inizio_tmp[6];
    char orario_fine_tmp[6];
    int posti_liberi_tmp;

    for(i=0; i<cont_proiezioni-1; i++){
        min=i;
        for(j=i+1;j<cont_proiezioni;j++){
            if(val_data(orario_inizio[j]) < val_data(orario_inizio[min])){  // confronta il valore ordinabile dell'orario di inizio (vedi val_data())
                min=j;
            }
        }

        // titolo
        strcpy(titolo_tmp, titolo[i]);
        strcpy(titolo[i], titolo[min]);
        strcpy(titolo[min], titolo_tmp);
        // sala 
        strcpy(sala_tmp, sala[i]);
        strcpy(sala[i], sala[min]);
        strcpy(sala[min], sala_tmp);
        // orario inizio 
        strcpy(orario_inizio_tmp, orario_inizio[i]);
        strcpy(orario_inizio[i], orario_inizio[min]);
        strcpy(orario_inizio[min], orario_inizio_tmp);
        // orario fine 
        strcpy(orario_fine_tmp, orario_fine[i]);
        strcpy(orario_fine[i], orario_fine[min]);
        strcpy(orario_fine[min], orario_fine_tmp);
        // posti liberi
        posti_liberi_tmp = posti_liberi[i];
        posti_liberi[i] = posti_liberi[min];
        posti_liberi[min] = posti_liberi_tmp;
    }

}


/*
 * Converte l'orario da una stringa ad un intero, in modo da poter eseguire l'ordinamento
 */
int val_data(char a_orario[]){
    char orario[5];
    
    //00:00
    //01234
    
    //0000
    //0123

    // metto le cifre dell'orario passato nella funzione nel vettore orario[]
    orario[0]=a_orario[0];
    orario[1]=a_orario[1];
    orario[2]=a_orario[3];
    orario[3]=a_orario[4];
    orario[4]=0;

    return atoi(orario);
}


/*
 * Funzione di test per il riempimento dei vettori
 */
void test_riempimento(){
    strcpy(titolo[cont_proiezioni],"Blade Runner 2049\0");
    strcpy(sala[cont_proiezioni],"01A\0");
    strcpy(orario_inizio[cont_proiezioni],"20:00\0");
    strcpy(orario_fine[cont_proiezioni],"21:00\0");
    posti_liberi[cont_proiezioni]=50;
    cont_proiezioni++;
    strcpy(titolo[cont_proiezioni],"A qualcuno piacciono i cani\0");
    strcpy(sala[cont_proiezioni],"02A\0");
    strcpy(orario_inizio[cont_proiezioni],"21:00\0");
    strcpy(orario_fine[cont_proiezioni],"22:00\0");
    posti_liberi[cont_proiezioni]=60;
    cont_proiezioni++;
}


/*
 * Verifica che non si sia raggiunto lo spazio nei vettori
 * Se restituisce 1, ok
 */
int controllo_spazio(){
    if(cont_proiezioni<NPROIEZIONI){
        return 1;
    }else{
        return 0;
    }
}


/*
 * Pulizia schermo
 */
void clean(){
    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #else
        system("clear");
    #endif
}
        

int valida_orario(char orario[]){
    char w_orario[6];

    // la funzione strtok modifica la stringa 
    // copio la stringa globale in una locale
    strcpy(w_orario, orario);
    
    // verifica il formato HH:mm
    // non valido se
    if(
        // prima e secondo carattere non numeri 
        !isdigit(w_orario[0]) ||  
        !isdigit(w_orario[1]) ||  
        // terzo carattere non :
        w_orario[2]!=':' ||
        // quarto e quinto carattere non numeri
        !isdigit(w_orario[3]) ||  
        !isdigit(w_orario[4])
    ){
        return -1;
    }

    char *token;
    int ora;
    int min;
    // formato orario: HH:mm
    token = strtok(w_orario, ":");
    ora = atoi(token);
    token = strtok(NULL, ":");
    min = atoi(token);
    
    if((ora>=0&&ora<24)&&(min>=0&&min<60)){
        return 1;
    }else{
        return -1;
    }
}


int durata_to_fine(char a_orario_inizio[], int minuti_durata, char a_orario_fine[]){
    char w_orario_inizio[6];
    char o_orario_fine[6];
    char *token;
    int ora_inizio;
    int minuti_inizio;
    int tmp;

    /* 
     * se orario inizio = 20:31
     * ora_inizio=20
     * minuti_inizio=31
     */
    strcpy(w_orario_inizio, a_orario_inizio);
    token = strtok(w_orario_inizio, ":");
    ora_inizio = atoi(token);
    token = strtok(NULL, ":");
    minuti_inizio = atoi(token);

    // trovo i minuti mancanti all'ora successiva dalla durata, incremento l'ora, azzero i minuti e li tolgo dalla durata
    tmp = 60-minuti_inizio;
    if(minuti_durata < tmp){
        // il film dura meno dei minuti che mancano all'ora successiva
        minuti_inizio+=minuti_durata;
    }else{
        minuti_durata-=tmp; //tolgo minuti dalla durata
        minuti_inizio=0; // azzero minuti
        ora_inizio++; // incremento ora

        ora_inizio+=minuti_durata/60; // aggiungo il numero di ore rimanenti
        minuti_durata-=(minuti_durata/60)*60; // rimuovo il numero di ore (in minuti) dalla durata mancante della proiezione

        // minuti_durata dovrebbe essere <60, quindi aggiungo ai minuti senza ulteriori azioni
        minuti_inizio+=minuti_durata;
    }
    
    sprintf(o_orario_fine, "%2d:%2d", ora_inizio, minuti_inizio);  // int->str
    
    // rimpiazza gli spazi con 0
    int i;
    for(i=0; i<5; i++){
        if(isspace(o_orario_fine[i])){
           o_orario_fine[i]='0';
        }
    }
    strcpy(a_orario_fine, o_orario_fine);
    return 0;  

}


int controlla_sovrapposizioni(char a_orario_inizio[], char a_orario_fine[], char a_sala[], int esclusa){
    
    // orario_*_proiezione -> proiezione da inserire
    // orario_*_proiezione_x -> proiezione da confrontare nel ciclo
    int valido=1;
    char orario_inizio_proiezione[6];
    char orario_fine_proiezione[6];
    char orario_inizio_proiezione_x[6];
    char orario_fine_proiezione_x[6];
    
    strcpy(orario_inizio_proiezione, a_orario_inizio);
    strcpy(orario_fine_proiezione, a_orario_fine);

    // dato che bisogna solo confrontare i due orari (non bisogna sapere la differenza), converto l'orario in float
    // sostituisco : con .
    orario_inizio_proiezione[2]='.';
    orario_fine_proiezione[2]='.';

    // Ciclo di controllo sovrapposizioni
    int i;
    for(i=0; i<cont_proiezioni; i++){
        if(i!=esclusa && !strcmp(sala[i], a_sala)){ // sala uguale
            
            // copio inizo e fine proiezione in variabili locali
            strcpy(orario_inizio_proiezione_x, orario_inizio[i]);
            strcpy(orario_fine_proiezione_x, orario_fine[i]);
            
            // sostituisco : con . per conversione -> float
            orario_inizio_proiezione_x[2]='.';
            orario_fine_proiezione_x[2]='.';

            // casi di non validità
            if(
                //   |    proiezione_x   |
                //             |    proiezione   |
                (atof(orario_inizio_proiezione)<=atof(orario_fine_proiezione_x) && atof(orario_inizio_proiezione)>atof(orario_inizio_proiezione_x)) ||
                
                //             | proiezione_x  |
                //   | proiezione |
                (atof(orario_fine_proiezione)>=atof(orario_inizio_proiezione_x) && atof(orario_inizio_proiezione)<atof(orario_fine_proiezione_x))
            ){
                return -1;
            }
        }
    }
    return 0;
}

int controlla_orario(char a_orario_inizio[], char a_orario_fine[]){
    // utilizza lo stesso metodo di controlla_sovrapposizioni per verificare che l'orario di inizio non sia antecedente all'orario di fine

    int valido=1;
    char orario_inizio_proiezione[6];
    char orario_fine_proiezione[6];

    strcpy(orario_inizio_proiezione, a_orario_inizio);
    strcpy(orario_fine_proiezione, a_orario_fine);

    orario_inizio_proiezione[2]='.';
    orario_fine_proiezione[2]='.';
    
    if(atof(orario_fine_proiezione) < atof(orario_inizio_proiezione)){
        return -1;
    }else{
        return 0;
    }
 }


/*
 * Socket ricerca film
 */
 
int fsearch(char query[]){

    // Windows richiede cose
    #if defined(_WIN32) || defined(_WIN64)
        WSADATA wsa;
        if (WSAStartup(MAKEWORD(2,2), &wsa) != 0){
            printf("Errore durante l'inizializzazione del sottosistema socket di windows. Installa linux\n");
            return -1;
        }
        // creazione client socket
        int client_socket;
        if((client_socket = socket(AF_INET, SOCK_STREAM, 0))==INVALID_SOCKET){
            printf("Errore durante la creazione del socket (%d)\n\n", WSAGetLastError());
        }
    #else
        int client_socket = socket(AF_INET, SOCK_STREAM, 0);
    #endif

    // indirizzo server
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(PORTA); // porta
    server_address.sin_addr.s_addr = inet_addr(IP);  // indirizzo ip


    //                              socket, struttura dell'indirizzo "convertita", grandezza dell'indirizzo
    int connection_status = connect(client_socket, (struct sockaddr *) &server_address, sizeof(server_address));

    // se errore, connect() restituisce -1
    if(connection_status==-1){
        printf("Errore durante la connessione al servizio.\n\n");
        return -1;
    }
    
    // richiesta durata (invio del titolo del film)
    char richiesta[256];
    strcpy(richiesta, query);
    //send(client_socket, richiesta, sizeof(richiesta), 0);
    send(client_socket, richiesta, strlen(richiesta), 0);

    // ricezione della durata
    char durata[256];
    recv(client_socket, durata, sizeof(durata), 0); // recv() mette i dati ricevuti della var. durata
   
    // chiusura socket
    #if defined(_WIN32) || defined(_WIN64)  
        closesocket(client_socket);
        WSACleanup();
    #else
        close(client_socket);
    #endif

    return atoi(durata); // restituisce la durata in minuti (int)
}
