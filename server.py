import socket
import requests

host = "0.0.0.0" # Indirizzo di ascolto
port = 6947 # porta

omdb_api_key = '21005e30' # API KEY di omdb
data_URL = 'http://www.omdbapi.com/?apikey='+omdb_api_key

# creazione socket
server_socket = socket.socket()

# attiva il socket
server_socket.bind((host, port))

# ascolta per 2 connessioni contemporaneamente
server_socket.listen(2)
while True:
    # Accetta la connessione
    conn, address = server_socket.accept()

    print(f"Connessione accettata da {str(address)}")

    # Riceve il titolo del film
    titolo = conn.recv(256).decode()
    
    # parametri della richiesta HTTP all'endpoint
    params = {
        't':titolo
    }

    try:
        # esegue la richiesta http, salva la risposta come json
        info = requests.get(data_URL, params=params).json()
        print(info)
        # ricava la durata dalla risposta
        durata = info.get('Runtime')
        print(durata)

        # durata consiste nel numero di minuti + ' min'
        # si considera quindi ogni carattere della stringa fino al quart'ultimo
        durata = durata[:-4]
        
        # si invia al client la durata codificata in ascii
        conn.send(durata.encode())

        # si chiude il socket
        conn.close()
    except:
        conn.send("-1".encode())
        conn.close()
