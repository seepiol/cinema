# Cinema Multisala
Scrivere un programma per la gestione delle prenotazioni in un cinema multisala per un certo giorno.

> ATTENZIONE
> Per una corretta compilazione, su Dev-c++ andare a Tools > Compiler Options e nella textbox in basso (riguardante il linker) aggiungere:
`-lws2_32` in modo tale da importare correttamente winsock2

> I due sorgenti (`multisala_grafica_w32.c` e `multisala.c`) sono esattamente identici, l'unica differenza consiste nell'encoding dei caratteri ascii esteso per la grafica, che se compilati con dev-c++ risulterebbero illeggibili

## Funzioni 
- Creazione Proiezione
- Visualizzazione Proiezione in ordine cronologico in base all'orario di inizio
- Modifica Proiezione
- Eliminazione Proiezione
- Modifica posti in sala rimanenti
- Ricerca (in base a titolo e sala)


## Creazione proiezione
Funzione `aggiungi()`

Consiste nella richiesta in input da parte dell'utente di:
- Titolo del film proiettato
- Codice identificativo di 3 lettere della sala
- Orario di inizio della proiezione
- Orario di termine della proiezione / durata in minuti / recupero automatico della durata in base al titolo (vedi #durata)
- Numero di posti "liberi", cioè prenotabili, in sala

Eventuali sovrapposizioni di proiezioni della stessa sala sono verificate dalla funzione `controlla_sovrapposizioni`.

### Durata
Per determinare l'orario di fine della proiezione, ci sono tre modi:
1) Inserire semplicemente l'orario di fine della proiezione
2) Utilizzare l'API semplificata
3) Inserire la durata in minuti del film

Il metodo dell'API semplificata consiste in una conessione tramite un raw socket ad un server remoto, che utilizzando le REST API pubbliche di Open Movie DataBase (https://www.omdbapi.com/), dato il titolo restituisce la durata della pellicola. 

Dato che le operazioni di esecuzione della richiesta http e dell'elaborazione della risposta in formato JSON, in C e senza l'aiuto di librerie esterne (che avrebbero influenzato la portabilità del programma) ho deciso di creare un "intermediario" tra le api di omdb e il programma. 

Il server socket, scritto in python, è contenuto nel file `server.py` e può essere eseguito localmente o, come da impostazione di default, su un apposito server pubblico disponibile all'indirizzo ip '45.77.63.134:6940`

È possibile modificare le impostazioni di default cambiando il valore delle costanti globali `IP` e `PORTA`

## Visualizzazione
funzione `visualizza()`

Oltre a visualizzare tutte le proiezioni, di tutte le sale, in ordine cronologico (la struttura dati viene ordinata ogni volta che questa funzione viene chiamata), può opzionalmente fornire un campo di input per la selezione di una specifica proiezione.

L'ordinamento avviene grazie alla funzione `ordina()`, che trasforma l'orario di inizio di ciascuna proiezione in un intero ordinabile, e ordina utilizzando l'algoritmo "selection sort"

## Modifica
funzione `modifica()`

Permette di modificare ogni attributo della proiezione.

## Eliminazione
funzione `elimina()`

Permette di selezionare e di eliminare una proiezione, per poi eseguire uno shift dell'intera struttura dati per evitare spreco di spazio.

## Prenotazioni
funzione `prenotazioni()`

Dopo aver selezionato una proiezione, permette di decrementare di uno il numero di posti liberi (premendo invio) o di decrementare di n posti, inserendo il valore di n.

Nel caso in cui il valore di n sia maggiore di quello dei posti disponibili, permette all'utente di decidere se accettare il numero di prenotazioni uguale ai posti disponibili, ignorando quelle in eccesso-
